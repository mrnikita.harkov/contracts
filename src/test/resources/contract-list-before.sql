delete from contract;

INSERT INTO contract(id, email, contract_number, installment_plan,
first_payment, second_payment, third_payment, first_sum, second_sum, third_sum) VALUES
(1,'TestFirst@mail.com','11111ED',true,null,null,null,0,0,0),
(2,'TestSecond@mail.com','2222145',true,'2020-08-15','2020-08-16','2020-08-17',111.50,222.50,333.50),
(3,'TestThird@mail.com','ED525',true,'2020-08-14','2020-08-15','2020-08-16',111.50,222.50,333.50),
(4,'TestFourth@mail.com','349OLR',true,'2020-08-13','2020-08-14','2020-08-15',111.50,222.50,333.50),
(5,'TestFifth@mail.com','Nsd1257',false,'2020-08-15','2020-08-16','2020-08-17',111.50,222.50,333.50);