package ee.club.contracts.controller;

import ee.club.contracts.ContractsApplication;
import ee.club.contracts.domain.Contract;
import ee.club.contracts.repository.ContractRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = ContractsApplication.class
)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
class ContractControllerTest {
    private static final String URL = "/contracts";
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ContractRepository contractRepository;

    @BeforeEach
    void fillTestData() {
        Contract contract = new Contract();
        contract.setId(1);
        contract.setName("first");
        contract.setPhone("55546582");
        contract.setContractNumber("11111ED");
        contractRepository.save(contract);
        contract.setId(2);
        contract.setName("second");
        contract.setPhone("5254325");
        contract.setContractNumber("2222145");
        contractRepository.save(contract);
        contract.setId(3);
        contract.setName("third");
        contract.setPhone("444444");
        contract.setContractNumber("ED525");
        contractRepository.save(contract);
        contract.setId(4);
        contract.setName("fourth");
        contract.setPhone("666666");
        contract.setContractNumber("349OLR");
        contractRepository.save(contract);
        contract.setId(5);
        contract.setName("fifth");
        contract.setPhone("123456789");
        contract.setContractNumber("Nsd1257");
        contractRepository.save(contract);
    }

    @Test
    void getContracts_ShouldReturnContent_WhenDoingGetRequest() throws Exception {
        mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content").exists());
    }

    @Test
    void getContracts_ShouldReturn5Contracts_WhenDoingGetRequest() throws Exception {
        mockMvc.perform(get(URL)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.numberOfElements").value(5));
    }

    @Test
    void addContract_ShouldAddContract_WhenGivenDataInPost() throws Exception {
        mockMvc.perform(post(URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\": \"post request test\",\"phone\": \"252020\"},\"contractNumber\": \"666\"}"))
                .andExpect(status().isOk());
        /*TODO:Need to check added value*/
        mockMvc.perform(get("/contracts")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[0].id").value(6))
                .andExpect(jsonPath("$.content[0].name").value("post request test"))
                .andExpect(jsonPath("$.content[0].phone").value("252020"))
                .andExpect(jsonPath("$.content[0].contractNumber").value("666"));
    }

    @Test
    void getContract() {
    }

    @Test
    void updateContract() {
    }

    @Test
    void deleteContract() {
    }
}