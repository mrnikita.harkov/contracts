package ee.club.contracts.service.mail;

import ee.club.contracts.ContractsApplication;
import ee.club.contracts.service.mail.properties.MailProperties;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = ContractsApplication.class
)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(value = {"classpath:contract-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Sql(value = {"classpath:contract-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class EmailSenderTest {
    private final EmailSender emailSender;
    private final static LocalDate DAY_FOR_TEST = LocalDate.of(2020, 8, 12);
    @MockBean
    private MailProperties properties;

    @Autowired
    EmailSenderTest(EmailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Test
    void sendEmails_ShouldSendTwoMails_WhenThreeDaysLeftBeforePaymentDay() {
        emailSender.sendEmails();
        Mockito.verify(properties, Mockito.times(2)).sendEmail(
                ArgumentMatchers.contains("@"),
                ArgumentMatchers.contains("Lepping"),
                ArgumentMatchers.anyString()
        );
        //TODO: Should pass without changing date (need to mock today date in sendEmails method)
    }
}