package ee.club.contracts.service.mail.properties;

import ee.club.contracts.domain.Contract;
import ee.club.contracts.dto.PaymentContractDto;
import ee.club.contracts.repository.ContractRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-test.properties")
@Sql(value = {"classpath:contract-list-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@Sql(value = {"classpath:contract-list-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
class PaymentMailsTest {
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private PaymentMails paymentMails;
    private final static LocalDate DAY_FOR_TEST = LocalDate.of(2020, 8, 12);


    @Test
    void getEmailsForPayment_ShouldReturnEmptyListOfEmails_WhenDateIsNull() {
        List<PaymentContractDto> expected = new ArrayList<>();
        List<PaymentContractDto> actual = paymentMails.getEmailsForPayment(null);
        assertEquals(expected, actual);
    }

    @Test
    void getEmailsForPayment_ShouldChangeInstallmentPlanToFalse_WhenPaymentIsNull() {
        paymentMails.getEmailsForPayment(DAY_FOR_TEST);
        Contract actual = contractRepository.findById(1L).orElse(new Contract());
        assertFalse(actual.isInstallmentPlan());
    }

    @Test
    void getEmailsForPayment_ShouldNotReturnContracts_WhenInstallmentPlanIsFalseAndDatesAreMatches() {
        List<PaymentContractDto> actual = paymentMails.getEmailsForPayment(DAY_FOR_TEST);
        assertFalse(actual.stream()
                .map(PaymentContractDto::getEmail)
                .anyMatch("TestFifth@mail.com"::equals));
    }

    @Test
    void getEmailsForPayment_ShouldReturnOnePaymentEmail_WhenThereIsThreeDaysLeftBeforePayment() {
        List<PaymentContractDto> expected = Arrays.asList(
                new PaymentContractDto("TestSecond@mail.com", "2222145", LocalDate.of(2020, 8, 15), 111.5),
                new PaymentContractDto("TestThird@mail.com", "ED525", LocalDate.of(2020, 8, 15), 222.5),
                new PaymentContractDto("TestFourth@mail.com", "349OLR", LocalDate.of(2020, 8, 15), 333.5)
        );
        List<PaymentContractDto> actual = paymentMails.getEmailsForPayment(DAY_FOR_TEST);
        assertEquals(expected, actual);
    }
}