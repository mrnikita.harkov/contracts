package ee.club.contracts.service.mail.properties;

import ee.club.contracts.domain.Contract;
import ee.club.contracts.dto.PaymentContractDto;
import ee.club.contracts.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class PaymentMails {
    private static final Logger log = Logger.getLogger(PaymentMails.class.getName());
    private static final int DAYS_BEFORE_PAYMENT = 3;
    private List<PaymentContractDto> mails;
    private final ContractRepository contractRepository;

    @Autowired
    public PaymentMails(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    public List<PaymentContractDto> getEmailsForPayment(LocalDate today) {
        List<Contract> contracts = contractRepository.findByInstallmentPlan(true);
        if (contracts.size() == 0)
            return new ArrayList<>();
        log.log(Level.INFO, "getEmailsForPayment method started at: " + new Date() +
                "\nCount of contracts: " + contracts.size());
        mails = new ArrayList<>();
        addToPaymentMails(contracts, today);
        return mails;
    }

    private void addToPaymentMails(List<Contract> contracts, LocalDate today){
        //TODO: Make Payments dates not null by default
        for (Contract contract : contracts) {
            if (contract.getFirstPayment() == null) {
                disableInstallmentPlan(contract);
            } else if (today==null) {
                log.log(Level.WARNING,"Passed Date equals null in PaymentMails.class, addToPaymentMails:method");
                break;
            } else {
                if (checkPaymentForDaysDifference(today, contract.getFirstPayment())) {
                    addToPaymentMails(contract, contract.getFirstPayment(), contract.getFirstSum());
                    if (checkForNextPaymentDate(contract.getSecondPayment())) {
                        disableInstallmentPlan(contract);
                    }
                }
                if (checkPaymentForDaysDifference(today, contract.getSecondPayment())) {
                    addToPaymentMails(contract, contract.getSecondPayment(), contract.getSecondSum());

                    if (checkForNextPaymentDate(contract.getThirdPayment())) {
                        disableInstallmentPlan(contract);
                    }
                }
                if (checkPaymentForDaysDifference(today, contract.getThirdPayment())) {
                    addToPaymentMails(contract, contract.getThirdPayment(), contract.getThirdSum());
                    disableInstallmentPlan(contract);
                }
            }
        }
    }


    private void disableInstallmentPlan(Contract contract) {
        contract.setInstallmentPlan(false);
        contractRepository.save(contract);
    }

    private boolean checkForNextPaymentDate(LocalDate paymentDate) {
        return paymentDate == null;
    }

    private void addToPaymentMails(Contract contract, LocalDate paymentDate, double sum) {
        mails.add(new PaymentContractDto(
                contract.getEmail(),
                contract.getContractNumber(),
                paymentDate,
                sum));
    }

    private boolean checkPaymentForDaysDifference(LocalDate today, LocalDate paymentDate) {
        return daysDifference(today, paymentDate) == DAYS_BEFORE_PAYMENT;
    }

    private long daysDifference(LocalDate today, LocalDate paymentDate) {
        return DAYS.between(today, paymentDate);
    }
}
