package ee.club.contracts.service.mail.properties;

import ee.club.contracts.dto.PaymentContractDto;
import org.springframework.stereotype.Service;

import java.time.format.DateTimeFormatter;

@Service
public class PaymentMailMessage {
    public String generateMessage(PaymentContractDto contract) {
        return String.format(
                "<body style='color: black;'>" +
                        " <div style='margin: 10px; padding: 15px;'>" +
                        "        <div>Уважаемый клиент,</div>" +
                        "        <p>Напоминаем Вам что <b><i>%s</i></b> у Вас следующая оплата по договору <b><i>%s</i></b> на сумму: %s€</p>" +
                        "        <p>Вы можете произвести оплату по банку:</p>" +
                        "        <ul>" +
                        "            <li>Swedbank EE442200221017216080</li>" +
                        "            <li>AS Seb BANK EE541010220272311229</li>" +
                        "            <li>AS Luminor Bank EE711700017000596219</li>" +
                        "        </ul>" +
                        "         <p>В пояснении просим указать номер договора.</p>" +
                        "        <div>" +
                        "            Также Вы можете прийти в офис и оплатить картой или наличными." +
                        "            <ul style='padding-left: 15px;'>" +
                        "                <li>Это автоматическое сообщение, пожалуйста не отвечайте на него.</li>" +
                        "                <li>Если Вы уже произвели оплату, проигнорируйте это сообщение.</li>" +
                        "            </ul>" +
                        "            Ждем вас с заказом на отдых! <span><img src='https://cdn0.iconfinder.com/data/icons/flat-travel-icons-1/48/64-512.png' style='width: 2em; height: 2em;'></span>" +
                        "        </div>" +
                        "        <div style='width: 600px; height: 10px; border-top: 2px solid black; position: absolute; margin-top: 10px;'></div>" +
                        "        <footer>" +
                        "            Clubs Travel OU <br>" +
                        "            10111 Narva mnt.7 D,korpus B, 4.korrus, Tallinn <br>" +
                        "            mob. 59817307 <br>" +
                        "            tel. 613 06 34 <br>" +
                        "            www.clubstravel.ee <br>" +
                        "<img src='https://clubstravel.ee/wp-content/uploads/2019/03/logo-2.png'>" +
                        "        </footer>" +
                        "    </div>" +
                        "</body>",
                contract.getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                contract.getNumber(),
                contract.getSum()
        );
    }
}
