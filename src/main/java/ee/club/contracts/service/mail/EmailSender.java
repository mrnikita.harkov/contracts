package ee.club.contracts.service.mail;

import ee.club.contracts.dto.PaymentContractDto;
import ee.club.contracts.service.mail.properties.MailProperties;
import ee.club.contracts.service.mail.properties.PaymentMailMessage;
import ee.club.contracts.service.mail.properties.PaymentMails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class EmailSender {
    private final MailProperties properties;
    private final PaymentMailMessage message;
    private final PaymentMails mails;
    private final static String SCHEDULE_TIME_EVERY_DAY_1PM = "0 0 13 * * ?"; //16:30 Tallinn Time

    @Autowired
    public EmailSender(MailProperties properties, PaymentMailMessage message, PaymentMails mails) {
        this.properties = properties;
        this.message = message;
        this.mails = mails;
    }

    @Scheduled(cron = SCHEDULE_TIME_EVERY_DAY_1PM)
    public void sendEmails() {
        LocalDate today = LocalDate.now();
        List<PaymentContractDto> emailsForPayment = mails.getEmailsForPayment(today);
        emailsForPayment.forEach(contract -> {
            String paymentMessage = message.generateMessage(contract);
            String subject = "Lepping " + contract.getNumber();
            properties.sendEmail(contract.getEmail(), subject, paymentMessage);
        });
    }
}
