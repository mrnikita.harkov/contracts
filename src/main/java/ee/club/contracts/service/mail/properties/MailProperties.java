package ee.club.contracts.service.mail.properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import java.util.logging.Logger;

@Service
public class MailProperties {
    private static final Logger log = Logger.getLogger(MailProperties.class.getName());
    private final JavaMailSender javaMailSender;
    @Value("${spring.mail.username}")
    private String mailUsername;

    @Autowired
    public MailProperties(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendEmail(String to, String subject, String message) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
            messageHelper.setFrom(mailUsername);
            messageHelper.setTo(to);
            messageHelper.setSubject(subject);
            messageHelper.setText(message, true);
        };
        try {
            javaMailSender.send(messagePreparator);
        } catch (MailException ex) {
            log.throwing("MailSender", "sendEmail", ex);
        }
    }
}
