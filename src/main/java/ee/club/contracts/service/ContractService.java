package ee.club.contracts.service;

import ee.club.contracts.domain.Contract;
import ee.club.contracts.repository.ContractRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Service
public class ContractService {
    private final ContractRepository contractRepository;

    public ContractService(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    public Page<Contract> findContracts(String name, String phone, String contractNumber, Pageable pageable) {
        if (!StringUtils.isEmpty(name) || !StringUtils.isEmpty(phone) || !StringUtils.isEmpty(contractNumber)) {
            return contractRepository.findByFilter(name, phone, contractNumber, pageable);
        }
        return contractRepository.findAll(pageable);
    }
}
