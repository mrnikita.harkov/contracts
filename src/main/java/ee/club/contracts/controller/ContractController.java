package ee.club.contracts.controller;

import com.fasterxml.jackson.annotation.JsonView;
import ee.club.contracts.domain.Contract;
import ee.club.contracts.domain.Views;
import ee.club.contracts.repository.ContractRepository;
import ee.club.contracts.service.ContractService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/contracts")
public class ContractController {
    private final ContractService contractService;
    private final ContractRepository contractRepository;

    @Autowired
    public ContractController(ContractService contractService, ContractRepository contractRepository) {
        this.contractService = contractService;
        this.contractRepository = contractRepository;
    }

    @GetMapping
    @JsonView(Views.IdContract.class)
    public Page<Contract> getContracts(
            @RequestParam(name = "name", required = false, defaultValue = "") String name,
            @RequestParam(name = "phone", required = false, defaultValue = "") String phone,
            @RequestParam(name = "contractNumber", required = false, defaultValue = "") String contractNumber,
            @PageableDefault(sort = {"id"}, direction = Sort.Direction.DESC) Pageable pageable) {
        return contractService.findContracts(name, phone, contractNumber, pageable);
    }

    @PostMapping("/add")
    @PreAuthorize("hasRole('USER')")
    public Contract addContract(@RequestBody Contract contract) {
        return contractRepository.save(contract);
    }

    @GetMapping("{id}")
    @JsonView(Views.FullContract.class)
    public Contract getContract(@PathVariable("id") Contract contract) {
        return contract;
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('USER')")
    public Contract updateContract(
            @PathVariable("id") Contract contractFromDb,
            @RequestBody Contract contract) {
        BeanUtils.copyProperties(contractFromDb, contract, "id");
        return contractRepository.save(contractFromDb);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('USER')")
    public void deleteContract(@PathVariable("id") Contract contract) {
        contractRepository.delete(contract);
    }
}
