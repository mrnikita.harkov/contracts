package ee.club.contracts.controller;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/")
public class LoginController {
    @GetMapping
    public String login(Principal principal) throws Exception {
        if (principal!=null && ((Authentication)principal).isAuthenticated()) {
            return "forward:/contracts";
        }
        return "login";
    }
}
