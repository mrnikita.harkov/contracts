package ee.club.contracts.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@EqualsAndHashCode
public class PaymentContractDto {
    private String email;
    private String number;
    private LocalDate date;
    private double sum;
}
