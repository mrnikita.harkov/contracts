package ee.club.contracts.repository;

import ee.club.contracts.domain.Contract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ContractRepository extends JpaRepository<Contract, Long> {
    Page<Contract> findAll(Pageable pageable);
    List<Contract> findByInstallmentPlan(boolean condition);

    @Query("SELECT c FROM Contract c " +
            "WHERE upper(c.name) like concat('%', upper(?1), '%') and " +
            "upper(c.phone) like concat('%', upper(?2), '%') and " +
            "upper(c.contractNumber) like concat('%', upper(?3), '%') " +
            "ORDER BY c.endDate ASC")
    Page<Contract> findByFilter(String name, String phone, String contractNumber, Pageable pageable);
}
