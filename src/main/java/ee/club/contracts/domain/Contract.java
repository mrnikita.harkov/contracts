package ee.club.contracts.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table
@Data
public class Contract implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonView(Views.IdContract.class)
    private long id;
    @JsonView(Views.IdContract.class)
    private String name;
    @JsonView(Views.IdContract.class)
    private boolean installmentPlan;
    @JsonView(Views.IdContract.class)
    private String phone;
    @JsonView(Views.IdContract.class)
    private String email;
    @JsonView(Views.IdContract.class)
    private String contractNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    @JsonView(Views.IdContract.class)
    private LocalDate startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    @JsonView(Views.IdContract.class)
    private LocalDate endDate;

    @JsonView(Views.FullContract.class)
    private String personalCode;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    @JsonView(Views.FullContract.class)
    private LocalDate firstPayment;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    @JsonView(Views.FullContract.class)
    private LocalDate secondPayment;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd.MM.yyyy")
    @JsonView(Views.FullContract.class)
    private LocalDate thirdPayment;

    @JsonView(Views.FullContract.class)
    private double firstSum = 0.0;
    @JsonView(Views.FullContract.class)
    private double secondSum = 0.0;
    @JsonView(Views.FullContract.class)
    private double thirdSum = 0.0;

    @JsonView(Views.FullContract.class)
    private String comment;
}
