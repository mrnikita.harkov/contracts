package ee.club.contracts.domain;

public final class Views {
    public interface IdContract {}
    public interface FullContract extends IdContract {}
}
